from django.urls import re_path
from .views import my_experience
from .views import my_education

#url for app
urlpatterns = [
    re_path(r'^my_ex/$', my_experience , name='my_experience'),
    re_path(r'^my_ed/$', my_education , name='my_education')
]